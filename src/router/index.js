import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/store'
import Auth from '../views/Auth.vue'
import Search from '../views/Search.vue'
import Favorites from '../views/Favorites.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/search',
      component: Search,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      component: Auth,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/favorites',
      component: Favorites,
      meta: {
        requiresAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/search')
  } else {
    next()
  }
})

export default router
